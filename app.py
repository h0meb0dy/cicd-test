from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return 'hello BoB from h0meb0dy'

@app.route('/add')
def add():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return str(a + b)

@app.route('/sub')
def sub():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return str(a - b)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8166)