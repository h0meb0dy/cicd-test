import unittest
import app

class Test(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_add(self):
        rv = self.app.get('/add?a=3&b=2')
        assert b'5' in rv.data

    def test_sub(self):
        rv = self.app.get('/sub?a=3&b=2')
        assert b'1' in rv.data

if __name__ == '__main__':
    unittest.main()